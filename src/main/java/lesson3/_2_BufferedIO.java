package lesson3;

import java.io.*;

import static lesson3.Shared.*;

public class _2_BufferedIO {

   public static void main(String[] args) throws IOException {

      deleteFile(filePath);

      try (OutputStream out = new BufferedOutputStream(new FileOutputStream(filePath))) {
         for (int i = 0; i < 42; i++) {
            log("writing " + i);
            out.write(i);
         }
      }

      System.out.println();


      try (InputStream in = new BufferedInputStream(new FileInputStream(filePath))) {
         while (true) {
            int read = in.read();
            log("reading " + read);

            if (read == -1) {
               break;
            }
         }
      }
   }
}
