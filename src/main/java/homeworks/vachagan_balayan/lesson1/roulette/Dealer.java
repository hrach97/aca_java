package homeworks.vachagan_balayan.lesson1.roulette;

import lesson1._roulette.interfaces.IDealer;
import lesson1._roulette.interfaces.IPlayer;
import lesson1._roulette.interfaces.IRoulette;

public class Dealer implements IDealer {
   private final IRoulette roulette;

   public Dealer(IRoulette roulette) {
      this.roulette = roulette;
   }

   @Override
   public void takeBet(IPlayer player, IRoulette.Color color, int amount) {
      player.debit(amount);
      IRoulette.IResult result = roulette.spin();
      if (result.getColor().equals(color)) {
         player.credit(amount * 2);
      }
   }
}
