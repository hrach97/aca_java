package homeworks.vachagan_balayan.lesson1.chessboard;

import lesson1._chess.interfaces.IChessBoard;
import lesson1._chess.interfaces.IChessFigure;
import lesson1._chess.interfaces.IPosition;

public class ChessBoard implements IChessBoard {
   @Override
   public boolean canMove(IPosition position1, IPosition position2) {
      return false;
   }

   @Override
   public void move(IPosition position1, IPosition position2) {

   }

   @Override
   public IChessFigure getFigureAt(IPosition position) {
      return null;
   }
}
