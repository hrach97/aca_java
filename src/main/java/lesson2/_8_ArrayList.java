package lesson2;

import java.util.Arrays;



public class _8_ArrayList {

   static class MyList<T> {

      private static final Object[] EMPTY = {};

      private Object[] arr = EMPTY;
      private int size = 0;

      public void add(T element) {
         ensureCap(size + 1);
         arr[size++] = element;
      }

      private void ensureCap(int minSize) {
         if (minSize > arr.length - 1) {
            int newSize = minSize * 2;
            System.out.println("expanding to size : " + newSize);
            Object[] newArr = new Object[newSize];
            for (int i = 0; i < size; i++) {
               newArr[i] = arr[i];
            }
            this.arr = newArr;
         }
      }


      public T get(int index) {
         if (index >= 0 && index < size) {
            return (T) arr[index];
         } else {
            return null;
         }
      }

      public T remove(int index) {
         if (index >= 0 && index < size) {
            T result = get(index);

            for (int i = index + 1; i < arr.length; i++) {
               arr[i - 1] = arr[i];
            }

            arr[size] = null;
            size--;
            return result;
         } else {
            throw new IndexOutOfBoundsException();
         }

      }

      public void print() {
         System.out.println(Arrays.toString(arr));
      }
   }

   public static void main(String[] args) {
      MyList<String> list = new MyList<>();

      for (int i = 0; i < 5; i++) {
         list.add(String.valueOf(i));
      }

      list.print();

      for (int i = 0; i < 5; i++) {
         System.out.println(list.get(i));
      }

      list.print();

      list.remove(1);

      list.print();

   }
}
