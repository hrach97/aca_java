package lesson2;

public class _4_BigO {

   // O(1) constant time
   public static void printFirstItem(int[] items) {
      System.out.println(items[0]);
   }

   // O(n) linear
   public static void printAllItems(int[] items) {
      for (int item : items) {
         System.out.println(item);
      }
   }

   // O(n^2) quadratic time
   public static void printAllPossibleOrderedPairs(int[] items) {
      for (int firstItem : items) {
         for (int secondItem : items) {
            System.out.println(firstItem + ", " + secondItem);
         }
      }
   }



   public static void main(String[] args) {
      int[] numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

      printFirstItem(numbers);
      System.out.println();
      printAllItems(numbers);
      System.out.println();
      printAllPossibleOrderedPairs(numbers);


   }
}
